package fr.colin.passwordtoclipboard.app;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.logging.Logger;

import com.melloware.jintellitype.HotkeyListener;
import com.melloware.jintellitype.JIntellitype;

public class PassToClipKeyPressListener implements HotkeyListener {
    
    private JIntellitype itltype;   // to listen to shortcuts
    private boolean leave;
    
    PassToClipKeyPressListener() {
        // get global instance
        itltype = JIntellitype.getInstance();
        leave = false;
        
        // register hotkeys
        // 0x70 should be F1 on windows, 112 in decimal
        // 0x71 should be F2 on windows, 113 in decimal
        // 0x51 should be Q on windows, 81 in decimal 
        itltype.registerHotKey(1, JIntellitype.MOD_CONTROL, 112);
        itltype.registerHotKey(2, JIntellitype.MOD_CONTROL, 113);
        itltype.registerHotKey(3, JIntellitype.MOD_CONTROL + JIntellitype.MOD_SHIFT, 81);

        // register as a listener once thread is started
        itltype.addHotKeyListener(this);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public void onHotKey(int arg0) {
        if (arg0 == 1) {
            Clipboard clpbrd = Toolkit.getDefaultToolkit ().getSystemClipboard();
            clpbrd.setContents(new StringSelection("str1"), null);
        }
        else if (arg0 == 2) {
            Clipboard clpbrd = Toolkit.getDefaultToolkit ().getSystemClipboard();
            clpbrd.setContents(new StringSelection("str2"), null);
        }
        else if (arg0 == 3) {
            leave = true;
        }
        else {
            Logger.getLogger(PassToClipKeyPressListener.class.getName()).warning("Uncaught event : " + arg0);
        }
    }
    
    public void unregister() {
        itltype.cleanUp();
    }

    /**
     * should be returning true once we shoot CTRL + SHIFT + Q
     * @return
     */
    public boolean leave() {
        return leave;
    }
}
