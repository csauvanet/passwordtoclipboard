package fr.colin.passwordtoclipboard.app;

import java.util.logging.Logger;

/**
 * purpose: have a class running on background on windows, catching some OS key press event (ex: CTRL+F1).
 * each key press is mapped to a dedicated string, hitting the shortcut will paste the string into the clipboard.
 * this is dedicated to be useful for your own password management
 *
 * TODO find a way to execute it as windows service
 * TODO have a different exit mechanism if windows service ?
 */
public class App 
{
    /**
     * this static main will launch the thread that should be running in the background of the OS
     * @param args
     */
    public static void main( String[] args )
    {
        // listener
        PassToClipKeyPressListener aListener = new PassToClipKeyPressListener();
        
        // keep this (and then listener) alive while not interrupted
        Logger.getLogger(App.class.getName()).info("Running (waiting)");
        while(!aListener.leave()) {
            try {
                // check every 5s if we need to exit
                // this won't prevent listener code to be executed
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
        // clean before leaving
        aListener.unregister();
    }
}
